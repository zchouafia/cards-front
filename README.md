# Cards front

## Purpose

The main purpose of this project is to demonstrate VueJS skills for a job interview

This project consumes the apis from Cards API project.

## Recommended Setup

- Node 18
- Vite 5
- [VS Code](https://code.visualstudio.com/) + [Vue - Official](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (previously Volar) and disable Vetur

## Run

### Locally
- `npm run dev`
- Go to http://localhost:9000

### Using docker
_Build and push a new image_ :

- `docker build -t zakch/cards-front:latest .`
- If not yet logged-in, `docker login -u "xxxxx" -p "xxxxx" docker.io`
- `docker push zakch/cards-front:latest`

_Run the latest version of cards front_ :

- `docker run -d -p 8080:8080 --name cards-front zakch/cards-front:latest`
- go to http://localhost:8080

## TODO
- Unit tests