declare type Card = {
    color: CardColor;
    number: CardNumber;
}