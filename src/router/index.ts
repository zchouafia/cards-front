import { createRouter, createWebHashHistory } from 'vue-router';
import Cards from '../views/Cards.vue';

const router = createRouter({
    history: createWebHashHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/cards",
            name: "cards",
            component: Cards
        }
    ]
});

export default router;