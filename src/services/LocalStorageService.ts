const LocalStorageService = {
    setItem: (key: string, value: any) => {
        const stringifiedValue: string = typeof value === 'string' ? value : JSON.stringify(value);
        localStorage.setItem(key, stringifiedValue);
    },

    getItem: <T>(key: string): T | null => {
        const value: string | null = localStorage.getItem(key);

        if (!value) {
            return null;
        }
        if (value.includes("{") || value.includes("[")) {
            return JSON.parse(value) as T;
        }
        return value as T;
    }
}

export default LocalStorageService;