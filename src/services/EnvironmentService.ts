const EnvironmentService = {
    getEnvVariables(): ImportMetaEnv {
        return import.meta.env;
    },

    getEnvVariable<T>(varName: string): T {
        return this.getEnvVariables()[varName];
    }
}

export default EnvironmentService;