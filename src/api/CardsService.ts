import axios from "axios";
import type { AxiosResponse } from "axios";
import EnvironmentService from "../services/EnvironmentService";
import EnvVariableConstants from "../constants/EnvVariableConstants";
import ApiConstantes from "../constants/ApiConstants";

const uri = EnvironmentService.getEnvVariable<string>(EnvVariableConstants.BACK_URL);
const url = `${uri}/${ApiConstantes.V1}/${ApiConstantes.CARDS_ENDPOINT}`;

const CardsService = {
    getCards(size: number): Promise<AxiosResponse<Card[]>> {
        return axios.get(url, {params: {size}});
    },

    sortCards(cardsRequest: CardsRequest): Promise<AxiosResponse<Card[]>> {
        return axios.post(url, cardsRequest);
    }
}

export default CardsService;